public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Author author1 = new Author();
        Author author2 = new Author("trần văn ninh ","ninhtv@devcamp.edu.vn",'m');
        Book book1 = new Book();
        Book book2 = new Book("ninh",author2,200000,25);
        System.out.println(author1.toString());
        System.out.println(book1.toString());
        System.out.println(author2.toString());
        System.out.println(book2.toString());

    }
}
